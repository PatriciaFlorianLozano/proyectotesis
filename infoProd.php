<!--====================================
     Designer and developers
 -Ismael López Mejía: Developer
 -Luis Alfredo Hernández: Developer
 -Carlos Eduardo Alfaro: Designer
************CopyRight 2014****************
=======================================-->
<?php
include './library/configServer.php';
include './library/consulSQL.php';
?>
<?php include "./inc/header.php"; ?>
<!-- <header class="main-header">
    <div class="container">
        <div class="col-xs-12 col-sm-5">
            <ul class="header-menu">
                <li><a href="" class="active">Inicio</a></li>
                <li class="dropdown">
                    <a href="">Información <i class="fa fa-angle-down"></i></a>
                    <div class="dropdown-content">
                        <a class="link" href="" style="color: #00efaf"><strong>Visión</a></strong>
                        <a class="link" href="" style="color: #00efaf"><strong>Misión</a></strong>
                        <a class="link" href="" style="color: #00efaf"><strong>Objetivos</a></strong>
                        <a class="link" href="" style="color: #00efaf"><strong>Nosotros</a></strong>
                    </div>
                </li>
                <li class="dropdown">
                    <a href="">Operaciones Diarias <i class="fa fa-angle-down"></i></a>
                    <div class="dropdown-content">
                        <a class="link" href="registrarProducto.php" style="color: #00efaf"><strong>Registrar
                                Producto</a></strong>
                        <a class="link" href="registrar-venta.php" style="color: #00efaf"><strong>Registrar
                                Venta</a></strong>
                        <a class="link" href="products.php" style="color: #00efaf"><strong>Registar Carrito</a></strong>
                        <a class="link" href="" style="color: #00efaf"><strong>Nosotros</a></strong>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-2 text-center">
            <a href="" class="logo"><img src="img/logo-header.png"></a>
        </div>
        <div class="col-xs-12 col-sm-5">
            <ul class="header-menu">
                <li><a href="product.php">Productos</a></li>
                <li><a href="admin.php">Administracion</a></li>
                <li><a href="logout.php">Cerrar Sesión</a></li>
            </ul>
        </div>
    </div>
</header> -->
<section id="infoproduct">
    <div class="container">
        <div class="row">
            <?php
            $CodigoProducto = $_GET['CodigoProd'];
            $productoinfo = ejecutarSQL::consultar("select * from productos where CodigoProd='" . $CodigoProducto . "'");
            $fila = mysqli_fetch_array($productoinfo);
            echo '<div class="col-xs-12 col-sm-6">
                    <h3 class="text-left title">Información de producto</h3>
                    <br><br>
                    <h4 class="detail-item"><strong>Nombre: </strong>' . $fila['NombreProd'] . '</h4><br>
                    <h4 class="detail-item"><strong>Modelo: </strong>' . $fila['Modelo'] . '</h4><br>
                    <h4 class="detail-item"><strong>Marca: </strong>' . $fila['Marca'] . '</h4><br>
                    <h4 class="detail-item"><strong>Precio: </strong>S/' . $fila['Precio'] . '</h4>

                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <br><br><br>
                    <img class="product-img" src="archivos/img-products/' . $fila['Imagen'] . '">
                </div>

                <div class="col-xs-12 action-buttons null-padding-side text-center section-small">
                    <a href="index2.php" class="button button-primary"><i class="fa fa-mail-reply"></i>&nbsp;&nbsp;Regresar a la tienda</a> &nbsp;&nbsp;&nbsp;
                    <button value="' . $fila['CodigoProd'] . '" class="button button-primary button-sell"><i class="fa fa-shopping-cart"></i>&nbsp;&nbsp; Añadir al carrito</button>
                </div>';
            ?>
        </div>
    </div>
</section>
<footer class="col-xs-12 main-footer">
    <div class="container" style="max-width: 1000px;">
        <div class="content text-center">
            <span>Síguenos en</span>
            <a href="#" class="social-icon fa fa-facebook"></a>
            <a href="#" class="social-icon fa fa-twitter"></a>
        </div>
    </div>
</footer>
</body>
</html>
