<?php
include './library/configServer.php';
include './library/consulSQL.php';
?>
<?php include "./inc/header.php"; ?>
<section id="store">
    <br>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul id="store-links" class="nav nav-tabs" role="tablist">
                    <li role="presentation"><a href="#all-product" role="tab" data-toggle="tab"
                                               aria-controls="all-product" aria-expanded="false">Todos los productos</a>
                    </li>
                    <li role="presentation" class="dropdown active">
                        <a href="#" id="myTabDrop1" class="dropdown-toggle" data-toggle="dropdown"
                           aria-controls="myTabDrop1-contents" aria-expanded="false">Categorías <span
                                    class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents">
                            <!-- ==================== Lista categorias =============== -->
                            <?php
                            $categorias = ejecutarSQL::consultar("select * from categoria");
                            while ($cate = mysqli_fetch_array($categorias)) {
                                echo '
                                    <li>
                                        <a href="#' . $cate['CodigoCat'] . '" tabindex="-1" role="tab" id="' . $cate['CodigoCat'] . '-tab" data-toggle="tab" aria-controls="' . $cate['CodigoCat'] . '" aria-expanded="false">' . $cate['Nombre'] . '
                                        </a>
                                    </li>';
                            }
                            ?>
                            <!-- ==================== Fin lista categorias =============== -->
                        </ul>
                    </li>
                </ul>
                <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade" id="all-product" aria-labelledby="all-product-tab">
                        <br><br>
                        <div class="row">
                            <?php
                            $consulta = ejecutarSQL::consultar("select * from productos where Stock > 0");
                            $totalproductos = $consulta->num_rows;
                            if ($totalproductos > 0) {
                                while ($fila = mysqli_fetch_array($consulta)) {
                                    echo '
                                   <div class="col-xs-12 col-sm-6 col-md-4">
                                     <div class="col-xs-12 product-card null-padding-side">
                                     <div class="product-img" style="background-image: url(archivos/img-products/' . $fila['Imagen'] . ');">
                                     </div>
                                     <div class="col-xs-12 content bg-white">
                                       <div class="info col-xs-6 null-padding-side">
                                           <h4 class="title">' . $fila['Marca'] . '</h4>
                                           <h6 class="subtitle">' . $fila['NombreProd'] . '</h6>
                                             </div>
                                             <div class="price col-xs-6 null-padding-side text-right">
                                             <h4>S/' . $fila['Precio'] . '</h4>
                                             </div>
                                            <div class="col-xs-12 action-buttons null-padding-side text-center">
                                               <a href="infoProd.php?CodigoProd=' . $fila['CodigoProd'] . '" class="button button-primary"><i class="fa fa-plus"></i>&nbsp; Detalles</a>&nbsp;&nbsp;
                                               <button value="' . $fila['CodigoProd'] . '" class="button button-primary"><i class="fa fa-shopping-cart"></i>&nbsp; Añadir</button>
                                           </div>

                                         </div>
                                       </div>
                                       </div>

                                   ';
                                }
                            } else {
                                echo '<h2>No hay productos en esta categoria</h2>';
                            }
                            ?>
                        </div>
                    </div><!-- Fin del contenedor de todos los productos -->

                    <!-- ==================== Contenedores de categorias =============== -->
                    <?php
                    $consultar_categorias = ejecutarSQL::consultar("select * from categoria");
                    while ($categ = mysqli_fetch_array($consultar_categorias)) {
                        echo '<div role="tabpanel" class="tab-pane fade active in" id="' . $categ['CodigoCat'] . '" aria-labelledby="' . $categ['CodigoCat'] . '-tab"><br>';
                        $consultar_productos = ejecutarSQL::consultar("select * from productos where CodigoCat='" . $categ['CodigoCat'] . "' and Stock > 0");
                        $totalprod = $consultar_productos->num_rows;
                        if ($totalprod > 0) {
                            while ($prod = mysqli_fetch_array($consultar_productos)) {
                                echo '
                                      <div class="col-xs-12 col-sm-6 col-md-4">
                                        <div class="col-xs-12 product-card null-padding-side">
                                        <div class="product-img" style="background-image: url(archivos/img-products/' . $prod['Imagen'] . ');">
                                        </div>
                                        <div class="col-xs-12 content bg-white">
                                          <div class="info col-xs-6 null-padding-side">
                                              <h4 class="title">' . $prod['Marca'] . '</h4>
                                              <h6 class="subtitle">' . $prod['NombreProd'] . '</h6>
                                                </div>
                                                <div class="price col-xs-6 null-padding-side text-right">
                                                <h4>S/' . $prod['Precio'] . '</h4>
                                                </div>
                                               <div class="col-xs-12 action-buttons null-padding-side text-center">
                                                  <a href="infoProd.php?CodigoProd=' . $prod['CodigoProd'] . '" class="button button-primary"><i class="fa fa-plus"></i>&nbsp; Detalles</a>&nbsp;&nbsp;
                                                  <button value="' . $prod['CodigoProd'] . '" class="button button-primary"><i class="fa fa-shopping-cart"></i>&nbsp; Añadir</button>
                                              </div>

                                            </div>
                                          </div>
                                          </div>

                                      ';
                            }

                        } else {
                            echo '';
                        }
                        echo '</div>';
                    }
                    ?>
                    <!-- ==================== Fin contenedores de categorias =============== -->
                </div>
            </div>
        </div>
    </div>
</section>
<footer class="col-xs-12 main-footer">
    <div class="container" style="max-width: 1000px;">
        <div class="content text-center">
            <span>Síguenos en</span>
            <a href="#" class="social-icon fa fa-facebook"></a>
            <a href="#" class="social-icon fa fa-twitter"></a>
        </div>
    </div>
</footer>
<script>
    $(document).ready(function () {
        $('#store-links a:first').tab('show');
    });
</script>
</body>
</html>
