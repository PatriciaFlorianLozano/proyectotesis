<!DOCTYPE html>
<html lang="en">
<link rel="stylesheet" type="text/css" href="menudes.css">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>S.O.S Boutique</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">

    <!-- Custom Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
          rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'
          rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="css/animate.min.css" type="text/css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/creative.css" type="text/css">
    <style type="text/css">

        body {
            background-image: url(img/login-bg.png);
            background-repeat: no-repeat;
            background-size: cover;
            height: 100vh;
        }
    </style>

</head>

<body>
<a href="#">
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">S.O.S BOUTIQUE</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right" id="menu">
                    <li>
                        <a class="page-scroll" href="#about">Inicio</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="">Información</a>
                        <ul>
                            <li>
                                <a class="page-scroll" href="#" id="menu">Vision</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="#" id="menu">Mision</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="#" id="menu">Objetivos</a>
                            </li>
                            <!-- <li>
                                <a class="page-scroll" href="#" id="menu">Nosotros
                            </li> -->
                        </ul>
                    </li>

                    <li>
                        <a class="page-scroll" href="">OPERACIONES DIARIAS</a>
                        <ul>
                            <li>
                                <a class="page-scroll" href="catalogo.php" id="menu">Registrar Cliente</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="libro_diario.php" id="menu">Registrar Empleado</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="balance_comprobacion.php" id="menu">Registrar Venta</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="estado_resultados.php" id="menu">Registrar Compra</a>
                            </li>
                            <!-- <li>
                                <a class="page-scroll" href="asiento.php" id="menu">Asiento</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="libro_mayor.php" id="menu">Libro Mayor</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="balance_contable.php" id="menu">Balance Contable</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="cuentasxcobrar.php" id="menu">Cuentas por Cobrar</a>
                            </li>
                            <li>
                                <a class="page-scroll" href="Reporte1.php" id="menu">Estadisticas</a>
                            </li> -->

                        </ul>
                    <li>
                        <a class="page-scroll" href="#services">Servicios</a>
                    </li>
                    <li>

                    <li><a class="page-scroll" href="#contact">Encuentranos</a>
                    </li>
                    <li>
                        <a class="pae-scroll" href="LOGOUT.php">Cerrar Sesión</a>
                    </li>

                    <li>
                        <a class="dropdown-toggle" data-toggle="dropdown" title="Ayuda" href="index3.php">
                            <span class="glyphicon glyphicon-question-sign hidden-xs"></span>
                            <span class="visible-xs">Ayuda</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="archivos/Manual.pdf" target="_blank">
                                    <i class="fa fa-book" aria-hidden="true"></i>&nbsp; Documentación
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header>
        <section id="about">
            <div class="header-content">
                <!-- <div class="header-content-inner"> -->

            </div>
            <h2>LISTA DE PERSONAL</h2>
            </div>
            <table width="70%" border="6px" align="center">
                <br>
                <tr align="center">
                    <td>DNI</td>
                    <td>Nombre</td>
                    <td>Apellido</td>
                    <td>Direccion</td>
                    <td>E-mail</td>

                </tr>
                <tr>
                    <td>48197624</td>
                    <td>Patricia</td>
                    <td>Florian</td>
                    <td>Calle: Renovacion#286</td>
                    <td>Paty@hotmail.com</td>

                </tr>
                <tr>
                    <td>45378965</td>
                    <td>Segundo</td>
                    <td>Perez</td>
                    <td>Calle:Huayna Capac#366</td>
                    <td>Segand@hotmail.com</td>


                </tr>
                <tr>
                    <td>08784523</td>
                    <td>Rossen</td>
                    <td>Zavaleta</td>
                    <td>Calle: Alto Peru#234</td>
                    <td>Rojazavel@hotmail.com</td>
                </tr>
                <tr>
                    <td>49786524</td>
                    <td>Jose</td>
                    <td>Cabanilla</td>
                    <td>Av: Mansiche#21</td>
                    <td>Jcabanillas@hotmail.com</td>
                </tr>
                <!-- <tr>
                    <td>5</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>7</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>8</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>9</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                </tr> -->
            </table>

        </section>
        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="js/jquery.easing.min.js"></script>
        <script src="js/jquery.fittext.js"></script>
        <script src="js/wow.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="js/creative.js"></script>

</body>

</html>