<?php
ob_start();
?>

<?php
include("library/configServer.php");
include("library/consulSQL.php");
include("archivos/Cusuario.php");

session_start();
if (isset($_SESSION['id'])) {
    header('location:index2.php');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>S.O.S Boutique</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="./css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="./css/custom.css" rel="stylesheet">
    <link href="./css/icheck/flat/green.css" rel="stylesheet">

    <script src="./js/jquery.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/bootstrap.js"></script>
    
    <style type="text/css">

        body {
            background-image: url(img/login-bg.png);
            background-repeat: no-repeat;
            background-size: cover;
            height: 100vh;
        }

    </style>

</head>

<body class="account-body">
<div id="wrapper">
    <div id="login" class="animate form">
        <section class="login-page text-center">
            <form role="form" method="POST" action="index.php">

                <!--<h1>Iniciar Sesión</h1>-->
                <img class="logo-img" width="" src="img/logo-dark-img.png" height="">
                <ul class="text-center nav nav-tabs" role="tab">
                    <li class="nav-item col-xs-6 null-padding-side">
                        <a class="nav-link active" href="login.php" role="tab">Iniciar Sesión</a>
                    </li>
                    <li class="nav-item col-xs-6 null-padding-side">
                        <a class="nav-link" href="RegistrarUsuario.php" role="tab">Registrate</a>
                    </li>
                </ul>
                <input type="hidden" name="control" value="12345">
                <div class="item form-group form-field-box">
                    <!--<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Usuario:
                    </label>-->
                    <br>
                    <div class="col-md-12 col-sm-12 col-xs-12 null-padding-side">
                        <i><img class="field-icon" src="img/ic-user.png"></i>
                        <input type="email" id="user" name="user" required="required"
                               class="form-control col-md-7 col-xs-12 form-field" placeholder="Usuario">
                    </div>
                </div>
                <div class="item form-group form-field-box">
                    <!--<label for="password" class="control-label col-md-3 col-sm-3 col-xs-12">Contraseña: </label> -->
                    <br>
                    <div class="col-md-12 col-sm-12  col-xs-12 null-padding-side">
                        <i><img class="field-icon" src="img/ic-password.png"></i>
                        <input id="password" type="password" name="password"
                               class="form-control col-md-7 col-xs-12 form-field" required="required"
                               placeholder="Contraseña">
                    </div>
                </div>


                <div class="col-xs-12 null-padding-side text-center">
                    <button class="btn btn-lg btn-success btn-block send-button">INICIAR SESIÓN</button>
                    <span class="or">o</span>
                    <a class="fb-login" href=""><i><img src="img/ic-fb.png"></i>Inicia Sesión con Facebook</a>
                </div>

                <div class="col-xs-12">
                    <br/>
                    <div>
                        <h1><i class="glyphicon glyphicon-gift" style="font-size: 26px;"></i> S.O.S Boutique</h1>

                        <p>©2017 S.O.S Boutique</p>
                    </div>
                </div>
            </form>
            <!-- form -->


        </section>
        <!-- content -->
    </div>
</div>
<?php
if (isset($_POST["control"])) {
    $user = $_POST["user"];
    $password = $_POST["password"];
    if ($user && $password) {
        $cn = ejecutarSQL::conectar();
        $oUsuario = new Cusuario($cn);
        $rs = $oUsuario->Obtener_x_Usuario($user);
        if ($rs->num_rows > 0) {
            $rsp = $oUsuario->Obtener_x_Usuario_clave($user, $password);
            if ($rsp->num_rows > 0) {
                $row = mysqli_fetch_assoc($rsp);
                $_SESSION['id'] = $row['USU_Codigo'];
                $_SESSION['usuario'] = $row['USU_Login'];
                $_SESSION['tipo'] = $row['PER_Descripcion'];
                if ($_SESSION['tipo'] == "Administrador") {
                    header('location:index.php');
                } else {
                    header('location:index2.php');
                }
            } else {
                ?>

                <div class="alert alert-warning">
                    <a href="login.php" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Incorrecto!</strong> Contraseña incorrecta.
                </div>
                <?php
            }
        } else {
            ?>
            <div class="alert alert-warning">
                <a href="login.php" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Incorrecto!</strong> El Usuario no Existe.
            </div>
            <?php
        }
    } else {
        ?>
        <div class="alert alert-warning">
            <a href="login.php" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Incorrecto!</strong> Debe escribir Usuario y contraseña.
        </div>
        <?php
    }
}
?>

</body>

</html>

<script src="js/validator/validator.js"></script>
<script>
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
        .on('keyup blur', 'input', function () {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function (e) {
        e.preventDefault();
        var submit = true;
        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit)
            this.submit();
        return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function () {
        $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function () {
        validator.defaults.alerts = (this.checked) ? false : true;
        if (this.checked)
            $('form .alert').remove();
    }).prop('checked', false);
</script>
<?php
ob_end_flush();
?>
