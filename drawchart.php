<HTML>
<BODY>

<meta charset="utf-8">
<?php include "./inc/header.php"; ?>
<?php
require_once 'library/configServer.php';
require_once 'library/consulSQL.php';
require_once("Model/Random.php");

//Creamos un objeto de la clase randomTable
$cn = ejecutarSQL::conectar();
$rand = new Random($cn);
//insertamos un valor aleatorio
$rand->insertRandom();
//obtenemos toda la información de la tabla random
$rawdata = $rand->getAllInfo();

//nos creamos dos arrays para almacenar el tiempo y el valor numérico
$valoresArray = [];
$timeArray = [];

//en un bucle for obtenemos en cada iteración el valor númerico y
//el TIMESTAMP del tiempo y lo almacenamos en los arrays
for ($i = 0; $i < count($rawdata); $i++) {
    $valoresArray[$i] = $rawdata[$i][1];
    //OBTENEMOS EL TIMESTAMP
    $time = $rawdata[$i][2];
    $date = new DateTime($time);
    //ALMACENAMOS EL TIMESTAMP EN EL ARRAY
    $timeArray[$i] = $date->getTimestamp() * 1000;
}
?>
<div id="contenedor"></div>

<script src="https://code.jquery.com/jquery.js"></script>
<!-- Importo el archivo Javascript de Highcharts directamente desde su servidor -->
<script src="http://code.highcharts.com/stock/highstock.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script>

    chartCPU = new Highcharts.StockChart({
        chart: {
            renderTo: 'contenedor'
            //defaultSeriesType: 'spline'

        },
        rangeSelector: {
            enabled: false
        },
        scrollbar: {
            enabled: false
        },
        title: {
            text: 'Gráfica de la Boutique S.O.S'
        },
        xAxis: {
            type: 'datetime'
            //tickPixelInterval: 150,
            //maxZoom: 20 * 1000
        },
        yAxis: {
            minPadding: 0.2,
            maxPadding: 0.2,
            title: {
                text: 'Valores',
                margin: 10
            }
        },
        series: [{
            name: 'Valor',
            data: (function () {
                // generate an array of random data
                var data = [];
                <?php
                for($i = 0 ;$i < count($rawdata);$i++){
                ?>
                data.push([<?php echo $timeArray[$i];?>,<?php echo $valoresArray[$i];?>]);
                <?php } ?>
                return data;
            })()
        }],
        credits: {
            enabled: false
        },
        legend: {
            enabled: true,
            position:
                {
                    align: "right",
                    verticalAlign: "bottom",
                    x: -10,
                    y: -5
                }
        }
    });

</script>
          <div class="col-xs-12 col-sm-4 col-md-3 bg-white logo-box">
<img class="" src="img/cabecera.png" alt="" style="width: 266px">
</div>
</BODY>

</html>
