<?php
include("library/configServer.php");
include("library/consulSQL.php");
?>
<html>
<body>
<style type="text/css">

    .caja-texto {
        font-family: arial;
        font-weight: bold;
        font-size: 15px;
        color: #00265D;
    }

    .caja-valor {
        font-family: arial;
        font-weight: bold;
        font-size: 16px;
        color: #6699FF;
        position: absolute;
        text-align: right;
    }

    #calcular {
        background-color: #cccccc;
        border-radius: 10px;
        width: 40%;
        height: 400px;
        margin-top: 3%;
        margin-left: 30%;
    }

    #linkvercata {
        color: black;
        font-size: 30px;
    }
</style>


<?php
if (isset($_GET['producto']))
    $_SESSION['prod'] = $_GET['producto'];

function revisar_cantidad()
{

    global $cantidad;
    global $embolsado;
    global $valor;
    global $unidad;
    global $total;

    if (!isset($_SESSION['id_art']))
        $_SESSION['id_art'] = 0;

    if (!isset($_SESSION[$_SESSION['id_art']]))
        $_SESSION[$_SESSION['id_art']] = array();


    if ($cantidad >= 1) {
        if ($cantidad < 300) {
            // switch($_SESSION['prod']){

            // }
        }


        $parcial = $unidad;  //Valor parcial del articulo

        if ($embolsado == 1) {
            $valor = 0.10;  //Precio del embolsado individual
            $parcial += $valor;
        }
        $total = $parcial * $cantidad;  //Precio total del articulo

        return true;
    } elseif ($cantidad > 0 && $cantidad < 100) {
        echo "<script language='javascript'>alert('La cantidad mínima tiene que ser de 1 UND')</script>";
    }
    return false;
}


echo "<form action= 'otromas.php' method='POST' name='formulario' id='formulario'>";

if (!isset ($_POST['cantidad']))
    $cantidad = 0;   //Eleccion del embolsado
else
    $cantidad = $_POST["cantidad"];

if (!isset($_POST['embolsado']))
    $embolsado = 0;   //Eleccion del embolsado
else
    $embolsado = $_POST['embolsado'];

$opc = revisar_cantidad(); //Variable para saber si se a hecho pedido

echo "<div align=center id='calcular'>";

echo
    " <br> <br>
          <p class = 'caja-texto'>Calcule el precio de su artículo<br> 
        Cantidad: " . "<input type=text name =cantidad size=3 maxlength = 4  value = $cantidad  onkeypress='return ValidNum(event);'><br>";

if ($embolsado == 1)
    echo "Embolsado Individual (+0,10): <input type = checkbox name = embolsado value = 1 checked = true><br><br>";
else
    echo "Embolsado Individual (+0,10): " . "<input type = checkbox name = embolsado value = 1><br><br>";

echo "<input type ='submit' value ='CALCULAR'></p>";

echo "<input type = text name = precio value = '$total'   style = visibility:hidden >";
//echo "<input type = text name = articulo value = '$_SESSION[prod]'   style = visibility:hidden ";

if ($opc != false) {

    echo
    "<br><p class = 'caja-texto'>Precio de la Unidad:<br>";
    printf("<font size='5'>%.2f €<br></font>", $unidad);

    if ($valor > 0) {
        echo "Embolsado individual:<br>";
        printf("<font size='5'>%.2f €<br></font>", $valor);
    }

    echo "Precio Total (sin IVA):<br>";
    printf("<font size='6'>%.2f €<br><br></font>", $total);


    echo "<input type = 'button' name='agregar' value='Añadir a mi Pedido' onClick='validar1()'><br>";


}
echo "</div>";
echo "<br><a href='productos.php' id='linkvercata' align='center'>Ver cat&aacutelogo</a>";
echo "</form>";

?>


<!-- Esta función hace que en el Input text solo se puedan ingresar números   -->
<script language="javascript">

    function ValidNum(e) {
        var tecla = document.all ? tecla = e.keyCode : tecla = e.which;
        return ((tecla > 47 && tecla < 58) || tecla == 8);
    }

    function validar1() {
        formulario.agregar.disabled = true;

        document.formulario.action = "agregar.php";
        document.formulario.method = "POST";
        document.formulario.submit();
    }

</script>


</body>
</html>