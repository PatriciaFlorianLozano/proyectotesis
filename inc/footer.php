<footer class="col-xs-12 main-footer">
    <div class="container" style="max-width: 1000px;">
        <div class="content text-center">
            <span>Síguenos en</span>
            <a href="#" class="social-icon fa fa-facebook"></a>
            <a href="#" class="social-icon fa fa-twitter"></a>
        </div>
    </div>
</footer>

<!-- Modal carrito -->
<div class="modal fade modal-carrito" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
     style="padding: 20px;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <br>
            <p class="text-center"><i class="fa fa-shopping-cart fa-5x"></i></p>
            <p class="text-center">El producto se añadio al carrito</p>
            <p class="text-center">
                <button type="button" class="btn btn-primary " data-dismiss="modal">Aceptar</button>
            </p>
        </div>
    </div>
</div>

</body>

</html>
