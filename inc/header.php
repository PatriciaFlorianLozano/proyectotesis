<?php

session_start();
if (!isset($_SESSION['id'])) {
    header('location:index.php');
}

if (!isset($_SESSION['contador'])) {
    $_SESSION['contador'] = 0;
}
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>S.O.S Boutique</title>

    <!-- CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="./css/animate.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="./css/custom.css" rel="stylesheet">
    <link href="./style.css" rel="stylesheet">
    <link href="./css/icheck/flat/green.css" rel="stylesheet">

    <link href="./css/normalize.css" rel="stylesheet">
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
    <link href="./css/media.css" rel="stylesheet">

    <script src="./js/jquery.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/autohidingnavbar.min.js"></script>
    <script src="./js/main.js"></script>
    <script src="./js/carrito.js"></script>

    <script type="text/javascript" src="./libs/ajax.js"></script>
    <!-- Alertity -->
     <link rel="stylesheet" href="./libs/js/alertify/themes/alertify.core.css" />
 	<link rel="stylesheet" href="./libs/js/alertify/themes/alertify.bootstrap.css" id="toggleCSS" />
     <script src="./libs/js/alertify/lib/alertify.min.js"></script>
</head>

<body>

<!--

<header class="main-header">
    <div class="container">
        <div class="col-xs-12 col-sm-5">
            <ul class="header-menu">
                <li><a href="/" class="active">Inicio</a></li>
                <li class="dropdown">
                    <a>Información <i class="fa fa-angle-down"></i></a>
                    <div class="dropdown-content">
                        <a class="link" href="#" style="color: #00efaf"><strong>Visión</strong></a>
                        <a class="link" href="#" style="color: #00efaf"><strong>Misión</strong></a>
                        <a class="link" href="#" style="color: #00efaf"><strong>Objetivos</strong></a>
                        <a class="link" href="#" style="color: #00efaf"><strong>Nosotros</strong></a>
                    </div>
                </li>
                <li class="dropdown">
                    <a>Operaciones Diarias <i class="fa fa-angle-down"></i></a>
                    <div class="dropdown-content">
                        <a class="link" href="registrarProducto.php" style="color: #00efaf">
                            <strong>Registrar Producto</strong></a>
                        <a class="link" href="registrar-venta.php" style="color: #00efaf">
                            <strong>Registrar Venta</strong></a>
                        <a class="link" href="products.php" style="color: #00efaf">
                            <strong>Registar Carrito</strong></a>
                        <a class="link" href="drawchart.php" style="color: #00efaf">
                            <strong>Graficos</strong></a>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-2 text-center">
            <a href="/" class="logo"><img src="img/logo-header.png"></a>
        </div>

        <div class="col-xs-12 col-sm-5">
            <ul class="header-menu">
                <li><a href="product.php">Productos</a></li>
                <li><a href="admin.php">Administracion</a></li>
                <li><a href="logout.php">Cerrar Sesión</a></li>
            </ul>
        </div>
    </div>
</header>-->


<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"
               style="width: 100px; background-size: 100% 100%; background-image: url('img/logo-header.png')"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="index2.php">Inicio <span class="sr-only">(current)</span></a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">Información <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a class="link" href="#" style="color: #00efaf"><strong>Visión</strong></a></li>
                        <li><a class="link" href="#" style="color: #00efaf"><strong>Misión</strong></a></li>
                        <li><a class="link" href="#" style="color: #00efaf"><strong>Objetivos</strong></a></li>
                        <li><a class="link" href="#" style="color: #00efaf"><strong>Nosotros</strong></a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">Operaciones Diarias <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a class="link" href="registrarProducto.php" style="color: #00efaf">
                                <strong>Registrar Producto</strong></a></li>
                        <li><a class="link" href="registrar-venta.php" style="color: #00efaf">
                                <strong>Registrar Venta</strong></a></li>
                        <li><a class="link" href="products.php" style="color: #00efaf">
                                <strong>Registar Carrito</strong></a></li>
                        <li><a class="link" href="drawchart.php" style="color: #00efaf">
                                <strong>Graficos</strong></a></li>
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="product.php">Productos</a></li>
                <li><a href="admin.php">Administracion</a></li>
                <li><a href="logout.php">Cerrar Sesión</a></li>
                <li>
                  <a  class="dropdown-toggle" data-toggle="dropdown" title="Ayuda" href="index2.php">
                     <span class="glyphicon glyphicon-question-sign hidden-xs"></span>
                     <span class="visible-xs">Ayuda</span>
                     </a>
                     <ul class="dropdown-menu">
                     <li>
                        <a href="archivos/manual.pdf" id="ayuda" target="_blank" style="color: #00efaf" value="bind ctrl+F11" >
                           <i class="fa fa-book" aria-hidden="true"></i>&nbsp; Ayuda en Linea
                        </a>
                        </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
