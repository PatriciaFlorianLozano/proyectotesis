<?php


require_once 'library/configServer.php';
require_once 'library/consulSQL.php';
require_once 'Model/Producto.php';

$cn = ejecutarSQL::conectar();
$objProducto = new Producto($cn);
$resultado_producto = $objProducto->get()
?>


  <?php include "./inc/header.php"; ?>
<div class="container">

    <div class="page-header">
        <h1 align="center" class="title-color">Registro de Productos</h1>
        <h3 class="subtitle-color">Cliente: Patricia Florian Lozano</h3>
    </div>
    <div class="row">
        <div class="col-md-4">

            <div>Producto:
                <select name="cbo_producto" id="cbo_producto" class="col-md-2 form-control">
                    <option value="0">Seleccione un producto</option>
                    <?php foreach ($resultado_producto as $producto): ?>
                        <option value="<?php echo $producto['id'] ?>"><?php echo $producto['descripcion'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div>Cantidad:
                <input id="txt_cantidad" name="txt_cantidad" type="text" class="col-md-2 form-control"
                       placeholder="Ingrese cantidad" autocomplete="off"/>
            </div>
        </div>
        <div class="col-md-2">
            <div style="margin-top: 19px;">
                <button type="button" class="btn btn-success btn-agregar-producto bg-secondary">Agregar</button>
            </div>
        </div>
    </div>

    <br>
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">Productos</h3>
        </div>
        <div class="panel-body detalle-producto">
          <?php if (count($_SESSION['detalle']) > 0) { ?>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Descripci&oacute;n</th>
                        <th>Cantidad</th>
                        <th>Precio</th>
                        <th>Subtotal</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($_SESSION['detalle'] as $k => $detalle) {
                        ?>
                        <tr>
                            <td><?php echo $detalle['producto']; ?></td>
                            <td><?php echo $detalle['cantidad']; ?></td>
                            <td><?php echo $detalle['precio']; ?></td>
                            <td><?php echo $detalle['subtotal']; ?></td>
                            <td>
                                <button type="button" class="btn btn-sm btn-danger eliminar-producto"
                                        id="<?php echo $detalle['id']; ?>">Eliminar
                                </button>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            <?php } else { ?>
                <div class="panel-body"> No hay productos agregados</div>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <button type="button" class="btn btn-sm btn-default guardar-carrito">Guardar</button>
        </div>
    </div>
</div>
</body>
</html>
