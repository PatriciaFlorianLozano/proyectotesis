<?php

class CUsuario
{
    private $cn;
    private $total_consultas;

    public function __construct($c)
    {
        $this->cn = $c;
    }

    function Obtener_x_Codigo($user)
    {
        $this->total_consultas++;
        $resultado = mysqli_query($this->cn, "SELECT u.USU_Codigo,u.USU_Login,u.USU_Clave,p.PER_Codigo,p.PER_Descripcion from  usuario u inner join
 			permisousuario pu on USU_Codigo=PU_Codigo inner join permiso p
			on PU_Codigo=PER_Codigo where u.USU_Login='$user'");
        if (!$resultado) {
            echo 'Mysql Error: ' . mysqli_error($this->cn);
            exit;
        }
        return $resultado;
    }

    function Lista_Personal($id)
    {
        $this->total_consultas++;
        $resultado = mysqli_query($this->cn, "SELECT * FROM usuario u inner join persona p on u.PER_Codigo=p.PER_Codigo where u.USU_Codigo='$id';");
        if (!$resultado) {
            echo 'Mysql Error: ' . mysqli_error($this->cn);
            exit;
        }
        return $resultado;
    }

    function Obtener_x_id($id)
    {
        $this->total_consultas++;
        $resultado = mysqli_query($this->cn, "SELECT * FROM usuario where id='$id'");
        if (!$resultado) {
            echo 'Mysql Error: ' . mysqli_error($this->cn);
            exit;
        }
        return $resultado;
    }

    function ListaUsuarios()
    {
        $this->total_consultas++;
        $resultado = mysqli_query($this->cn, "SELECT * from usuario u inner join persona p on u.PER_Codigo=p.PER_Codigo");
        if (!$resultado) {
            echo 'Mysql Error: ' . mysqli_error($this->cn);
            exit;
        }
        return $resultado;
    }

    function Obtener_x_Usuario($user)
    {
        $this->total_consultas++;
        $resultado = mysqli_query($this->cn, "SELECT * FROM usuario u inner join permisousuario pu on u.USU_Codigo=pu.USUARIO_USU_Codigo inner join permiso p on pu.Permiso_PER_Codigo=p.PER_Codigo where USU_Login='$user'");
        if (!$resultado) {
            echo 'Mysql Error: 1 ' . mysqli_error($this->cn);
            exit;
        }
        return $resultado;
    }

    function Obtener_x_Usuario_clave($user, $pass)
    {
        $this->total_consultas++;
        $resultado = mysqli_query($this->cn, "SELECT * FROM usuario u inner join permisousuario pu 
            on u.USU_Codigo=pu.USUARIO_USU_Codigo inner join permiso p on pu.Permiso_PER_Codigo=p.PER_Codigo 
            where USU_Login='$user' and USU_Clave='$pass'");

        if (!$resultado) {
            echo 'Mysql Error: ' . mysqli_error($this->cn);
            exit;
        }

        return $resultado;
    }

    function insertar($usuario, $clave, $nombre, $apellido)
    {
        $sql1 = "INSERT into persona(PER_Nombre,PER_Apellido)";
        $sql1 = $sql1 . "value('$nombre''$apellido')";
        $rpta = mysqli_query($this->cn, $sql1);

        $resultado1 = mysqli_query("SELECT MAX(PER_Codigo) as resul FROM persona", $this->cn);
        $fila = $resultado1->fetch_array(MYSQLI_ASSOC);
        $PER_Codigo = $fila["resul"];

        $sql = "INSERT into usuario(USU_Login,USU_Clave,PER_Codigo)";
        $sql = $sql . "value('$usuario','$clave','$PER_Codigo')";
        $rpta = mysqli_query($this->cn, $sql);
        if (!$rpta) {
            mysqli_rollback($this->cn);
            return 0;
        }

        $resultado = mysqli_query($this->cn, "SELECT * from usuario where USU_Login='$usuario' and USU_Clave='$clave'");
        $fila = $resultado->fetch_array(MYSQLI_ASSOC);
        $ObtenerCodigo = $fila["USU_Codigo"];

        $sql2 = "INSERT into permisousuario(USUARIO_USU_Codigo,Permiso_PER_Codigo)";
        $sql2 = $sql2 . "value('$ObtenerCodigo','2')";
        $rpta2 = mysqli_query($this->cn, $sql2);
        if (!$rpta2) {
            mysqli_rollback($this->cn);
            return 0;
        }
    }

    function modificar($id, $usuario, $clave, $tipousuario)
    {
        $sql = "update usuario set Usuario='$usuario',Clave='$clave',tipousuario='$tipousuario' where idAutonumerico='$id'";
        $rpta = mysqli_query($this->cn, $sql);
        if (!$rpta) {
            mysqli_rollback($this->cn);
        }
        return 0;
    }

    function eliminar($id)
    {
        $sql = "DELETE FROM usuario WHERE idAutonumerico='$id'";
        $rpta = mysqli_query($this->cn, $sql);
        if (!$rpta) {
            mysqli_rollback($this->cn);

        }
        return 0;
    }
}
