<?php

class Random
{

    public $IDr = 0;
    protected $cn;

    public function __construct($c)
    {
        $this->cn = $c;
    }

    //inserta en la base de datos un nuevo registro en la tabla usuarios
    function insertRandom()
    {
        //Generamos un número entero aleatorio entre 0 y 100
        $ran = rand(0, 100);
        //Escribimos la sentencia sql necesaria respetando los tipos de datos
        $sql = "insert into random (valor) values (" . $ran . ")";
        $resultado = mysqli_query($this->cn, $sql);
        if (!$resultado) {
            die('Mysql Error: ' . mysqli_error($this->cn));
        }
        return $resultado;
    }

    function getAllInfo()
    {
        //Creamos la consulta
        $sql = "SELECT * FROM random;";

        $resultado = mysqli_query($this->cn, $sql);
        if (!$resultado) {
            die('Mysql Error: ' . mysqli_error($this->cn));
        }

        $rawdata = array();
        //guardamos en un array multidimensional todos los datos de la consulta
        $i = 0;
        while ($row = mysqli_fetch_array($resultado)) {
            //guardamos en rawdata todos los vectores/filas que nos devuelve la consulta
            $rawdata[$i] = $row;
            $i++;
        }

        return $rawdata;
    }
}

