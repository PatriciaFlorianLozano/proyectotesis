<?php

class Producto
{
    protected $cn;

    public function __construct($c)
    {
        $this->cn = $c;
    }

    function get()
    {
        $sql = "SELECT * FROM producto";
        $resultado = mysqli_query($this->cn, $sql);
        if (!$resultado) {
            die('Mysql Error: ' . mysqli_error($this->cn));
        }
        return $resultado;

    }

    function getById($id)
    {
        $sql = "SELECT * FROM producto WHERE id=$id";
        $resultado = mysqli_query($this->cn, $sql);
        if (!$resultado) {
            die('Mysql Error: ' . mysqli_error($this->cn));
        }
        return $resultado;
    }

    function guardarVenta()
    {
        $sql = "INSERT INTO venta(fecha) values (NOW())";
        $resultado = mysqli_query($this->cn, $sql);
        if (!$resultado) {
            die('Mysql Error: ' . mysqli_error($this->cn));
        }
        return $resultado;
    }

    function getUltimaVenta()
    {
        $sql = "SELECT LAST_INSERT_ID() as ultimo";
        $resultado = mysqli_query($this->cn, $sql);
        if (!$resultado) {
            die('Mysql Error: ' . mysqli_error($this->cn));
        }
        return $resultado;
    }

    function guardarDetalleVenta($idventa, $idproducto, $cantidad, $precio, $subtotal)
    {
        $sql = "INSERT INTO venta_detalle (idventa,idproducto,cantidad,precio,subtotal) values ($idventa,$idproducto,$cantidad,'$precio','$subtotal')";
        $resultado = mysqli_query($this->cn, $sql);
        if (!$resultado) {
            die('Mysql Error: ' . mysqli_error($this->cn));
        }

    }

    function getDetalleVenta($idventa)
    {
        $sql = "SELECT p.descripcion as producto, vd.cantidad as cantidad, vd.precio as precio FROM venta_detalle vd
		INNER JOIN producto p 
		ON vd.idproducto = p.id
		WHERE idventa = $idventa";

        $resultado = mysqli_query($this->cn, $sql);
        if (!$resultado) {
            die('Mysql Error: ' . mysqli_error($this->cn));
        }
        return $resultado;
    }

}                                                                                                          