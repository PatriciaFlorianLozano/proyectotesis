<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>S.O.S Boutique</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link href="css/icheck/flat/green.css" rel="stylesheet">

    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/jquery.min.js"></script>
</head>

<body>
<header class="main-header">
    <div class="container">
        <div class="col-xs-12 col-sm-5">
            <ul class="header-menu">
                <li><a href="" class="active">Inicio</a></li>
                <li class="dropdown">
                    <a href="">Información <i class="fa fa-angle-down"></i></a>
                    <div class="dropdown-content">
                        <a class="link" href="">Visión</a>
                        <a class="link" href="">Misión</a>
                        <a class="link" href="">Objetivos</a>
                        <a class="link" href="">Nosotros</a>
                    </div>
                </li>
                <li class="dropdown">
                    <a href="">Operaciones Diarias <i class="fa fa-angle-down"></i></a>
                    <div class="dropdown-content">
                        <a class="link" href="">Visión</a>
                        <a class="link" href="">Misión</a>
                        <a class="link" href="">Objetivos</a>
                        <a class="link" href="">Nosotros</a>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-2 text-center">
            <a href="" class="logo"><img src="img/logo-header.png"></a>
        </div>
        <div class="col-xs-12 col-sm-5">
            <ul class="header-menu">
                <li><a href="">Servicios</a></li>
                <li><a href="">Encuentrános</a></li>
                <li><a href="">Cerrar Sesión</a></li>
            </ul>
        </div>
    </div>
</header>

<!-- Banner -->
<section class="banner-home section" style="background-image: url(img/banner-home.jpg)">
    <div class="container va-middle">
        <h1 class="title va-">Bienvenido a la página web <br>de la <strong>Boutique SOS</strong></h1>
        <a href="" class="button text-uppercase"><strong>Obtén más información</strong></a>
    </div>
</section>
</body>

</html>
