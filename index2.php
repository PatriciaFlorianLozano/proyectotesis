<?php include "./inc/header.php"; ?>

<!-- Banner -->
<section class="banner-home section" style="background-image: url(img/banner-home.jpg)">
    <div class="container va-middle">
        <h2 class="title-home" align="left" style="font-size: 48px">Bienvenido a la página web</h2>
        <h2 class="title-home" align="left"><strong style="font-size: 48px">de la Boutique S.O.S</strong></h2>
        <div style="width: 100%; padding: 24px 0;">
            <a href="" class="button text-uppercase"><strong>Obten más información</strong></a>
        </div>

        <div style="width: 100%; padding: 24px 0;">
            <label style="color:#de5eff"><img class="alineadoTextoImagenCentro" src="img/ic-dress.png ">
                <strong style="font-size: 18px">&nbsp; Viste genial, Viste S.O.S</strong></label>
        </div>
    </div>
</section>

<section class="col-xs-12 novedades-productos">
    <div class="container" style="max-width: 960px;">
        <div class="row">
            <div class="col-xs-12 title-section text-center">
                <h1 class="title"><span>NOVEDADES</span> &nbsp; PRODUCTOS</h1>
                <hr>
            </div>
            <?php
            include 'library/configServer.php';
            include 'library/consulSQL.php';
            $consulta = ejecutarSQL::consultar("select * from productos where Stock > 0 limit 6");
            $totalproductos = $consulta->num_rows;
            if ($totalproductos > 0) {
                while ($fila = mysqli_fetch_array($consulta)) {
                    echo '
                         <div class="col-xs-12 col-sm-6 col-md-4">
                           <div class="col-xs-12 product-card null-padding-side">
                           <div class="product-img" style="background-image: url(archivos/img-products/' . $fila['Imagen'] . ');">
                           </div>
                           <div class="col-xs-12 content bg-white">
                             <div class="info col-xs-6 null-padding-side">
                                 <h4 class="title">' . $fila['Marca'] . '</h4>
                                 <h6 class="subtitle">' . $fila['NombreProd'] . '</h6>
                                   </div>
                                   <div class="price col-xs-6 null-padding-side text-right">
                                   <h4>S/' . $fila['Precio'] . '</h4>
                                   </div>
                                  <div class="col-xs-12 action-buttons null-padding-side text-center">
                                     <a href="infoProd.php?CodigoProd=' . $fila['CodigoProd'] . '" class="button button-primary"><i class="fa fa-plus"></i>&nbsp; Detalles</a>&nbsp;&nbsp;
                                     <button value="' . $fila['CodigoProd'] . '" class="button button-primary"><i class="fa fa-shopping-cart"></i>&nbsp; Añadir</button>
                                 </div>

                               </div>
                             </div>
                             </div>

                         ';
                }
            } else {
                echo '<h2>No hay productos registrados en la tienda</h2>';
            }
            ?>

        </div>
    </div>
</section>

<section class="col-xs-12 call-to-action null-padding" style="background-image: url('img/bg-cta.png')">
    <div class="col-xs-12 overlay null-padding-side"></div>
    <div class="container section text-center">
        <div class="col-xs-12 content">
            <i class="fa fa-users"></i>
            <h1 class="title">Regístrate</h1>
            <p>Registrese y hagase cliente de S.O.S Boutique para recibir las mejores ofertas y descuentos especiales de
                nuestros productos.</p>
            <a href="RegistrarUsuario.php" class="button button-primary">REGISTRARME</a>
        </div>
    </div>
</section>

<section class="col-xs-12 distribuidores bg-white">
    <div class="container" style="max-width: 1000px;">
        <div class="row">
            <div class="col-xs-12 title-section text-center">
                <h1 class="title"><span>NUESTRAS</span> &nbsp; MARCAS</h1>
                <hr>
            </div>
            <div class="col-xs-12 clientes null-padding-side">
                <div class="col-xs-12 col-sm-4 col-md-3 bg-white logo-box">
                    <img class="logo-img" src="img/logos-clientes/1.png" alt="">
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 bg-white logo-box">
                    <img class="logo-img" src="img/logos-clientes/2.png" alt="">
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 bg-white logo-box">
                    <img class="logo-img" src="img/logos-clientes/3.png" alt="">
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 bg-white logo-box">
                    <img class="logo-img" src="img/logos-clientes/4.png" alt="">
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 bg-white logo-box">
                    <img class="logo-img" src="img/logos-clientes/5.jpg" alt="">
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 bg-white logo-box">
                    <img class="logo-img" src="img/logos-clientes/6.jpg" alt="">
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 bg-white logo-box">
                    <img class="logo-img" src="img/logos-clientes/7.png" alt="">
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 bg-white logo-box">
                    <img class="logo-img" src="img/logos-clientes/8.png" alt="">
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 bg-white logo-box">
                    <img class="logo-img" src="img/logos-clientes/9.png" alt="">
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 bg-white logo-box">
                    <img class="logo-img" src="img/logos-clientes/10.png" alt="">
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 bg-white logo-box">
                    <img class="logo-img" src="img/logos-clientes/11.png" alt="">
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 bg-white logo-box">
                    <img class="logo-img" src="img/logos-clientes/12.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "./inc/footer.php"; ?>
