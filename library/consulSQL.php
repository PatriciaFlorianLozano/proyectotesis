<?php

/* Clase para ejecutar las consultas a la Base de Datos*/

class ejecutarSQL
{
    public static function conectar()
    {
        $enlace = mysqli_connect(SERVER, USER, PASS, BD);

        if (mysqli_connect_errno()) {
            echo "Error: No se pudo conectar a MySQL . " . PHP_EOL;
            echo "errno de depuración: " . mysqli_connect_errno() . PHP_EOL;
            echo "error de depuración: " . mysqli_connect_error() . PHP_EOL;
            exit;
        }

        mysqli_set_charset($enlace, 'utf8');
        return $enlace;
    }

    public static function consultar($query)
    {
        $conexion = ejecutarSQL::conectar();
        if (!$consul = mysqli_query($conexion, $query)) {
            die("Error en la consulta SQL ejecutada: %s\n" . mysqli_error($conexion));
        }
        return $consul;
    }
}

/* Clase para hacer las consultas Insertar, Eliminar y Actualizar */

class consultasSQL
{
    public static function InsertSQL($tabla, $campos, $valores)
    {
        if (!$consul = ejecutarSQL::consultar("insert into $tabla ($campos) VALUES($valores)")) {
            die("Ha ocurrido un error al insertar los datos en la tabla $tabla");
        }
        return $consul;
    }

    public static function DeleteSQL($tabla, $condicion)
    {
        if (!$consul = ejecutarSQL::consultar("delete from $tabla where $condicion")) {
            die("Ha ocurrido un error al eliminar los registros en la tabla $tabla");
        }
        return $consul;
    }

    public static function UpdateSQL($tabla, $campos, $condicion)
    {
        if (!$consul = ejecutarSQL::consultar("update $tabla set $campos where $condicion")) {
            die("Ha ocurrido un error al actualizar los datos en la tabla $tabla");
        }
        return $consul;
    }
}
