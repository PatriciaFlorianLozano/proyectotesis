-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-12-2017 a las 03:41:28
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_tesis`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `Nombre` varchar(30) NOT NULL,
  `Clave` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`Nombre`, `Clave`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3'),
('Paty', 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

CREATE TABLE `cargo` (
  `CAR_Codigo` int(11) NOT NULL,
  `CAR_Descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`CAR_Codigo`, `CAR_Descripcion`) VALUES
(1, 'Adminitrador'),
(2, 'usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `client_email` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cart`
--

INSERT INTO `cart` (`id`, `client_email`, `created_at`) VALUES
(1, 'patypatriciafl@gmail.com', '2017-11-06 11:36:24'),
(2, 'lesly.26igs@gmail.com', '2017-11-06 11:40:16'),
(3, 'paty_jk_1230@hotmail.com', '2017-11-13 16:00:23'),
(4, 'paty_jk_1230@hotmail.com', '2017-11-13 16:26:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cart_producto`
--

CREATE TABLE `cart_producto` (
  `id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  `q` float NOT NULL,
  `cart_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cart_producto`
--

INSERT INTO `cart_producto` (`id`, `producto_id`, `q`, `cart_id`) VALUES
(1, 5, 1, 1),
(2, 4, 1, 2),
(3, 6, 1, 2),
(4, 7, 6, 2),
(5, 6, 1, 3),
(6, 8, 1, 3),
(7, 10, 1, 3),
(8, 13, 2, 3),
(9, 6, 3, 4),
(10, 5, 1, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `CodigoCat` varchar(30) NOT NULL,
  `Nombre` varchar(30) NOT NULL,
  `Descripcion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`CodigoCat`, `Nombre`, `Descripcion`) VALUES
('4', 'Zapatos', 'Todo tipo de zapatos en general'),
('C1', 'Damas', 'Ropa para la mujer de hoy'),
('C2', 'Caballeros', 'Ropa para todo tipo de hombre'),
('C3', 'Niños', 'Grna variedad de ropa para niños');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `DNI` varchar(30) NOT NULL,
  `Nombre` varchar(30) NOT NULL,
  `NombreCompleto` varchar(70) NOT NULL,
  `Apellido` varchar(70) NOT NULL,
  `Clave` text NOT NULL,
  `Direccion` varchar(200) NOT NULL,
  `Telefono` int(20) NOT NULL,
  `Email` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`DNI`, `Nombre`, `NombreCompleto`, `Apellido`, `Clave`, `Direccion`, `Telefono`, `Email`) VALUES
('79864523', 'Lesly', 'Lesly Tatiana', 'Zeña Vergara', '12345678', 'Calle Pacasmayo # 345', 987645321, 'Lzena@hotmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `id` int(11) NOT NULL,
  `numeroventa` int(11) NOT NULL,
  `nombre` text NOT NULL,
  `imagen` text NOT NULL,
  `precio` text NOT NULL,
  `cantidad` text NOT NULL,
  `subtotal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compras`
--

INSERT INTO `compras` (`id`, `numeroventa`, `nombre`, `imagen`, `precio`, `cantidad`, `subtotal`) VALUES
(1, 1, 'zapatillas', 'zapatillas.jpg', '88', '1', 88),
(2, 1, 'pantalones', 'pantalones.jpg', '76', '5', 380),
(3, 2, 'zapatillas', 'zapatillas.jpg', '88', '1', 88),
(4, 3, 'botas', 'botas.jpg', '85', '1', 85),
(16, 4, 'zapatillas', 'zapatillas.jpg', '87.5', '1', 88),
(17, 5, 'vestido niña', 'vestido.jpg', '150', '1', 150),
(18, 6, 'zapatillas', 'zapatillas.jpg', '87.5', '2', 175),
(19, 7, 'vestido niña', 'vestido.jpg', '150', '5', 750),
(20, 7, 'zapatillas', 'zapatillas.jpg', '87.5', '5', 438),
(21, 7, 'botines', 'botines.jpg', '75', '1', 75),
(22, 8, 'botines', 'botines.jpg', '75', '3', 225),
(23, 8, 'vestido niña', 'vestido.jpg', '150', '1', 150),
(24, 9, 'zapatillas', 'zapatillas.jpg', '87.5', '1', 88),
(25, 10, 'zapatillas', 'zapatillas.jpg', '87.5', '1', 88),
(26, 10, 'botas', 'botas.jpg', '85', '1', 85),
(27, 11, 'vestido niña', 'vestido.jpg', '150', '1', 150),
(28, 12, 'zapatillas', 'zapatillas.jpg', '87.5', '1', 88),
(29, 12, 'vestido niña', 'vestido.jpg', '150', '1', 150),
(30, 13, 'zapatillas', 'zapatillas.jpg', '87.5', '1', 88),
(31, 14, 'vestido niña', 'vestido.jpg', '150', '2', 300),
(32, 15, 'botas', 'botas.jpg', '85', '1', 85),
(33, 15, 'Balerina', 'balerinas.jpg', '48', '2', 96);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta`
--

CREATE TABLE `cuenta` (
  `codigo_mayor` int(11) NOT NULL,
  `nombre_cuenta` varchar(60) NOT NULL,
  `descripcion` varchar(75) DEFAULT NULL,
  `tipo_cuenta` int(11) DEFAULT NULL,
  `er` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle`
--

CREATE TABLE `detalle` (
  `NumPedido` int(20) NOT NULL,
  `CodigoProd` varchar(30) NOT NULL,
  `CantidadProductos` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detallefactura`
--

CREATE TABLE `detallefactura` (
  `DF_Codigo` int(11) NOT NULL,
  `DF_FechaGenerada` datetime DEFAULT NULL,
  `DF_Descripcion` varchar(45) DEFAULT NULL,
  `DF_Valor` decimal(12,0) DEFAULT NULL,
  `Factura_FAC_Codigo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_libro_diario`
--

CREATE TABLE `detalle_libro_diario` (
  `id_movimiento` int(11) NOT NULL,
  `cuenta` int(11) NOT NULL,
  `deber` decimal(12,2) DEFAULT NULL,
  `haber` decimal(12,2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `EMP_Codigo` int(11) NOT NULL,
  `Persona_PER_Codigo` int(11) NOT NULL,
  `TipoEmpleado_TP_Codigo` int(11) NOT NULL,
  `USUARIO_USU_Codigo` int(11) NOT NULL,
  `Cargo_CAR_Codigo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `FAC_Codigo` int(11) NOT NULL,
  `FAC_FechaGenerada` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formapago`
--

CREATE TABLE `formapago` (
  `FP_Codigo` int(11) NOT NULL,
  `FP_Nombre` varchar(45) DEFAULT NULL,
  `FP_Descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libro_diario`
--

CREATE TABLE `libro_diario` (
  `id_movimiento` int(11) NOT NULL,
  `dia` int(11) NOT NULL,
  `mes` int(11) NOT NULL,
  `ano` int(11) NOT NULL,
  `partida` int(11) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `libro_diario`
--

INSERT INTO `libro_diario` (`id_movimiento`, `dia`, `mes`, `ano`, `partida`, `descripcion`) VALUES
(1, 16, 11, 2016, 1, 'Aqui tenemos nuestras cuentas'),
(2, 5, 5, 1970, 2, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago`
--

CREATE TABLE `pago` (
  `PAG_Codigo` int(11) NOT NULL,
  `PAG_FechaPago` datetime DEFAULT NULL,
  `Valor` decimal(12,0) DEFAULT NULL,
  `Factura_FAC_Codigo` int(11) NOT NULL,
  `FormaPago_FP_Codigo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso`
--

CREATE TABLE `permiso` (
  `PER_Codigo` int(11) NOT NULL,
  `PER_Descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permiso`
--

INSERT INTO `permiso` (`PER_Codigo`, `PER_Descripcion`) VALUES
(1, 'Administrador'),
(2, 'usuario'),
(3, 'Gerente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisousuario`
--

CREATE TABLE `permisousuario` (
  `PU_Codigo` int(11) NOT NULL,
  `USUARIO_USU_Codigo` int(11) NOT NULL,
  `Permiso_PER_Codigo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permisousuario`
--

INSERT INTO `permisousuario` (`PU_Codigo`, `USUARIO_USU_Codigo`, `Permiso_PER_Codigo`) VALUES
(1, 4, 1),
(2, 1, 1),
(3, 5, 2),
(4, 6, 1),
(5, 1, 1),
(6, 7, 2),
(7, 9, 2),
(8, 10, 2),
(9, 11, 2),
(10, 12, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `PER_Codigo` int(11) NOT NULL,
  `PER_Nombre` varchar(45) DEFAULT NULL,
  `PER_Apellido` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`PER_Codigo`, `PER_Nombre`, `PER_Apellido`) VALUES
(1, 'Segundo', 'Perez');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `precio` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `descripcion`, `precio`) VALUES
(4, 'vestido par niña', 150),
(5, 'botines para dama', 75),
(6, 'polera para dama', 50),
(7, 'sandalias zapatos', 52),
(8, 'botas sobre la rodilla', 85),
(9, 'zapatos dama', 45),
(10, 'balerina de flores', 48),
(12, 'esto es una polera de dama', 52),
(13, 'esto es un abrigo', 50.9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `CodigoProd` varchar(30) NOT NULL,
  `NombreProd` varchar(30) NOT NULL,
  `CodigoCat` varchar(30) NOT NULL,
  `Precio` decimal(30,2) NOT NULL,
  `Modelo` varchar(30) NOT NULL,
  `Marca` varchar(30) NOT NULL,
  `Stock` int(20) NOT NULL,
  `IdProveedor` varchar(30) NOT NULL,
  `Imagen` varchar(150) NOT NULL,
  `Nombre` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`CodigoProd`, `NombreProd`, `CodigoCat`, `Precio`, `Modelo`, `Marca`, `Stock`, `IdProveedor`, `Imagen`, `Nombre`) VALUES
('2', 'Polo para caballero', 'C2', '67.00', 'G-008', 'Gzuck', 19, '2', 'FB_IMG_1512357163645.jpg', ''),
('3', 'Vestido Black', 'C1', '56.00', 'B-001', 'Woman Fashion', 39, '1', 'descarga.jpg', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `IdProveedor` varchar(30) NOT NULL,
  `NombreProveedor` varchar(30) NOT NULL,
  `Direccion` varchar(200) NOT NULL,
  `Telefono` int(20) NOT NULL,
  `PaginaWeb` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`IdProveedor`, `NombreProveedor`, `Direccion`, `Telefono`, `PaginaWeb`) VALUES
('1', 'China Fashion', 'Lima', 987645123, 'www.ChinaF.com'),
('1', 'China Fashion', 'Lima', 987645123, 'www.ChinaF.com'),
('2', 'Shila Work', 'Chepen', 908765432, 'www.shilaWork.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `random`
--

CREATE TABLE `random` (
  `ID` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `tiempo` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `random`
--

INSERT INTO `random` (`ID`, `valor`, `tiempo`) VALUES
(1, 22, '2017-12-18 18:25:03'),
(2, 70, '2017-12-18 18:26:52'),
(3, 21, '2017-12-18 19:32:48'),
(4, 30, '2017-12-18 19:32:58'),
(5, 69, '2017-12-18 19:33:01'),
(6, 83, '2017-12-18 19:33:02'),
(7, 62, '2017-12-18 19:33:02'),
(8, 72, '2017-12-18 19:33:02'),
(9, 62, '2017-12-18 19:33:02'),
(10, 69, '2017-12-18 19:33:03'),
(11, 91, '2017-12-18 19:33:03'),
(12, 15, '2017-12-18 19:33:03'),
(13, 50, '2017-12-18 19:33:27'),
(14, 17, '2017-12-18 19:33:27'),
(15, 33, '2017-12-18 19:33:28'),
(16, 73, '2017-12-18 19:33:34'),
(17, 37, '2017-12-18 19:39:00'),
(18, 44, '2017-12-18 20:03:32'),
(19, 92, '2017-12-18 20:05:14'),
(20, 84, '2017-12-18 21:24:57'),
(21, 95, '2017-12-18 21:34:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoempleado`
--

CREATE TABLE `tipoempleado` (
  `TP_Codigo` int(11) NOT NULL,
  `TP_Descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_cuenta`
--

CREATE TABLE `tipo_cuenta` (
  `id_tipo_cuenta` int(11) NOT NULL,
  `nombre_tipo_cuenta` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_cuenta`
--

INSERT INTO `tipo_cuenta` (`id_tipo_cuenta`, `nombre_tipo_cuenta`) VALUES
(1, 'Activo'),
(2, 'Pasivo'),
(3, 'Capital');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `USU_Codigo` int(11) NOT NULL,
  `USU_Login` varchar(45) DEFAULT NULL,
  `USU_Clave` varchar(45) DEFAULT NULL,
  `PER_Codigo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`USU_Codigo`, `USU_Login`, `USU_Clave`, `PER_Codigo`) VALUES
(1, 'Grethell@hotmail.com', '123456789', 1),
(3, 'paty@sos.com', 'patricia', 2),
(4, 'patricia', '123456', 3),
(5, 'Violeta@hotmail.com', 'patricia', 4),
(6, 'Fiorella@hotmail.com', 'fiorella', 1),
(7, 'Grethell@hotmail.com', 'grethell', 6),
(8, 'Segand@hotmail.com', '12345678', 1),
(9, 'hihiihjijo', 'fg7ygyghyg', 1),
(10, 'Segundo', 'Quiroz', 1),
(11, 'Lzena@hotmail.com', '12345678', 1),
(12, 'pat@gmail.com', '123456', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `id` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`id`, `fecha`) VALUES
(1, '2017-12-02 20:51:43'),
(2, '2017-12-03 21:48:09'),
(3, '2017-12-04 13:26:10'),
(4, '2017-12-08 18:36:24'),
(5, '2017-12-11 10:16:26'),
(6, '2017-12-11 10:49:38'),
(7, '2017-12-11 12:14:25'),
(8, '2017-12-11 12:33:04'),
(9, '2017-12-11 13:06:58'),
(10, '2017-12-11 15:10:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `NumPedido` int(20) NOT NULL,
  `Fecha` varchar(150) NOT NULL,
  `NIT` varchar(30) NOT NULL,
  `Descuento` int(20) NOT NULL,
  `TotalPagar` decimal(30,2) NOT NULL,
  `Estado` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_detalle`
--

CREATE TABLE `venta_detalle` (
  `id` int(11) NOT NULL,
  `idventa` int(11) NOT NULL,
  `idproducto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` decimal(9,0) NOT NULL,
  `subtotal` decimal(9,0) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `venta_detalle`
--

INSERT INTO `venta_detalle` (`id`, `idventa`, `idproducto`, `cantidad`, `precio`, `subtotal`) VALUES
(1, 1, 6, 12, '50', '600'),
(2, 1, 8, 23, '85', '1955'),
(3, 1, 10, 12, '48', '576'),
(4, 2, 6, 12, '50', '600'),
(5, 2, 8, 23, '85', '1955'),
(6, 2, 4, 3, '150', '450'),
(7, 2, 9, 34, '45', '1530'),
(8, 3, 7, 12, '52', '624'),
(9, 3, 10, 3, '48', '144'),
(10, 3, 12, 4, '52', '208'),
(11, 4, 8, 12, '85', '1020'),
(12, 4, 9, 23, '45', '1035'),
(13, 4, 5, 34, '75', '2550'),
(14, 5, 5, 34, '75', '2550'),
(15, 5, 9, 56, '45', '2520'),
(16, 6, 6, 12, '50', '600'),
(17, 6, 8, 34, '85', '2890'),
(18, 6, 10, 34, '48', '1632'),
(19, 7, 7, 67, '52', '3484'),
(20, 7, 8, 76, '85', '6460'),
(21, 8, 4, 12, '150', '1800'),
(22, 8, 6, 12, '50', '600'),
(23, 9, 7, 12, '52', '624'),
(24, 9, 9, 34, '45', '1530'),
(25, 9, 6, 23, '50', '1150'),
(26, 9, 13, 34, '51', '1731'),
(27, 10, 8, 12, '85', '1020'),
(28, 10, 9, 12, '45', '540'),
(29, 10, 12, 12, '52', '624');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`Nombre`);

--
-- Indices de la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`CAR_Codigo`);

--
-- Indices de la tabla `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cart_producto`
--
ALTER TABLE `cart_producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`CodigoCat`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`DNI`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD PRIMARY KEY (`codigo_mayor`),
  ADD UNIQUE KEY `codigo_mayor` (`codigo_mayor`),
  ADD KEY `cuenta_ibfk_1` (`tipo_cuenta`);

--
-- Indices de la tabla `detalle`
--
ALTER TABLE `detalle`
  ADD KEY `NumPedido` (`NumPedido`),
  ADD KEY `CodigoProd` (`CodigoProd`);

--
-- Indices de la tabla `detallefactura`
--
ALTER TABLE `detallefactura`
  ADD PRIMARY KEY (`DF_Codigo`),
  ADD KEY `fk_DetalleFactura_Factura1_idx` (`Factura_FAC_Codigo`);

--
-- Indices de la tabla `detalle_libro_diario`
--
ALTER TABLE `detalle_libro_diario`
  ADD KEY `detalle_libro_diario_ibfk_1` (`id_movimiento`),
  ADD KEY `detalle_libro_diario_ibfk_2` (`cuenta`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`EMP_Codigo`),
  ADD KEY `fk_Empleado_Persona1_idx` (`Persona_PER_Codigo`),
  ADD KEY `fk_Empleado_TipoEmpleado1_idx` (`TipoEmpleado_TP_Codigo`),
  ADD KEY `fk_Empleado_USUARIO1_idx` (`USUARIO_USU_Codigo`),
  ADD KEY `fk_Empleado_Cargo1_idx` (`Cargo_CAR_Codigo`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`FAC_Codigo`);

--
-- Indices de la tabla `formapago`
--
ALTER TABLE `formapago`
  ADD PRIMARY KEY (`FP_Codigo`);

--
-- Indices de la tabla `libro_diario`
--
ALTER TABLE `libro_diario`
  ADD PRIMARY KEY (`id_movimiento`),
  ADD UNIQUE KEY `id_movimiento` (`id_movimiento`);

--
-- Indices de la tabla `pago`
--
ALTER TABLE `pago`
  ADD PRIMARY KEY (`PAG_Codigo`),
  ADD KEY `fk_Pago_Factura1_idx` (`Factura_FAC_Codigo`),
  ADD KEY `fk_Pago_FormaPago1_idx` (`FormaPago_FP_Codigo`);

--
-- Indices de la tabla `permiso`
--
ALTER TABLE `permiso`
  ADD PRIMARY KEY (`PER_Codigo`);

--
-- Indices de la tabla `permisousuario`
--
ALTER TABLE `permisousuario`
  ADD PRIMARY KEY (`PU_Codigo`),
  ADD KEY `fk_PermisoUsuario_USUARIO1_idx` (`USUARIO_USU_Codigo`),
  ADD KEY `fk_PermisoUsuario_Permiso1_idx` (`Permiso_PER_Codigo`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`PER_Codigo`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `random`
--
ALTER TABLE `random`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `tipoempleado`
--
ALTER TABLE `tipoempleado`
  ADD PRIMARY KEY (`TP_Codigo`);

--
-- Indices de la tabla `tipo_cuenta`
--
ALTER TABLE `tipo_cuenta`
  ADD PRIMARY KEY (`id_tipo_cuenta`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`USU_Codigo`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cargo`
--
ALTER TABLE `cargo`
  MODIFY `CAR_Codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `cart_producto`
--
ALTER TABLE `cart_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `detallefactura`
--
ALTER TABLE `detallefactura`
  MODIFY `DF_Codigo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `EMP_Codigo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `FAC_Codigo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `formapago`
--
ALTER TABLE `formapago`
  MODIFY `FP_Codigo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `libro_diario`
--
ALTER TABLE `libro_diario`
  MODIFY `id_movimiento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `pago`
--
ALTER TABLE `pago`
  MODIFY `PAG_Codigo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `permiso`
--
ALTER TABLE `permiso`
  MODIFY `PER_Codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `permisousuario`
--
ALTER TABLE `permisousuario`
  MODIFY `PU_Codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `PER_Codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `random`
--
ALTER TABLE `random`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `tipoempleado`
--
ALTER TABLE `tipoempleado`
  MODIFY `TP_Codigo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tipo_cuenta`
--
ALTER TABLE `tipo_cuenta`
  MODIFY `id_tipo_cuenta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `USU_Codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detallefactura`
--
ALTER TABLE `detallefactura`
  ADD CONSTRAINT `fk_DetalleFactura_Factura1` FOREIGN KEY (`Factura_FAC_Codigo`) REFERENCES `factura` (`FAC_Codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD CONSTRAINT `fk_Empleado_Cargo1` FOREIGN KEY (`Cargo_CAR_Codigo`) REFERENCES `cargo` (`CAR_Codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Empleado_Persona1` FOREIGN KEY (`Persona_PER_Codigo`) REFERENCES `persona` (`PER_Codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Empleado_TipoEmpleado1` FOREIGN KEY (`TipoEmpleado_TP_Codigo`) REFERENCES `tipoempleado` (`TP_Codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Empleado_USUARIO1` FOREIGN KEY (`USUARIO_USU_Codigo`) REFERENCES `usuario` (`USU_Codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pago`
--
ALTER TABLE `pago`
  ADD CONSTRAINT `fk_Pago_Factura1` FOREIGN KEY (`Factura_FAC_Codigo`) REFERENCES `factura` (`FAC_Codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Pago_FormaPago1` FOREIGN KEY (`FormaPago_FP_Codigo`) REFERENCES `formapago` (`FP_Codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `permisousuario`
--
ALTER TABLE `permisousuario`
  ADD CONSTRAINT `fk_PermisoUsuario_Permiso1` FOREIGN KEY (`Permiso_PER_Codigo`) REFERENCES `permiso` (`PER_Codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_PermisoUsuario_USUARIO1` FOREIGN KEY (`USUARIO_USU_Codigo`) REFERENCES `usuario` (`USU_Codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
