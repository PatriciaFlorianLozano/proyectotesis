<?php

session_start();
if (isset($_SESSION['id'])) {
    // remove all session variables
    session_unset();

    // destroy the session
    session_destroy();
    header('location:index.php');
} else {
    header('location:index.php');
}
